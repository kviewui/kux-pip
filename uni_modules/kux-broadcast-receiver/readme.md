# kux-broadcast-receiver
`kux-broadcast-receiver` 是一个原生安卓广播接收器插件，旨在简化开发者们在实现广播注册、监听的流程，让开发者们专注业务开发，提升业务开发效率。

## 插件特色
+ 添加广播动作
+ 注册监听器
+ 广播接收器
+ 自定义广播接收内容形式
+ 支持多种广播内容类型
+ 实现用途广泛

## 目录结构
<ol>
	<li><a href="#guide">基础</a>
		<ol>
			<li><a href="#guide_guide">入门使用</a></li>
		</ol>
	</li>
	<li><a href="#advanced">进阶</a>
		<ol>
			<li><a href="#advanced_BroadcastNameItem">获取电量变化</a></li>
		</ol>
	</li>
	<li><a href="#api">API</a>
		<ol>
			<li><a href="#api_addAction">addAction</a></li>
			<li><a href="#api_registerReceiver">registerReceiver</a></li>
			<li><a href="#api_onReceiver">onReceiver</a></li>
			<li><a href="#api_unregisterReceiver">unregisterReceiver</a></li>
		</ol>
	</li>
	<li><a href="#interface">自定义类型</a>
	</li>
	<li><a href="#unierrors">错误码规范</a>
	</li>
</ol>

<a id="guide"></a>
## 基础
<a id="guide_guide"></a>
### 入门使用
+ **创建实例**

	```
	import { useBroadcastReceiver } from '@/uni_modules/kux-broadcast-receiver';
	import { KuxBroadcastReceiver } from '@/uni_modules/kux-broadcast-receiver/utssdk/interface';

	const kuxBroadcastReceiver: KuxBroadcastReceiver = useBroadcastReceiver();
	```
+ **添加广播动作**

	```
	import { AddActionOptions } from '@/uni_modules/kux-broadcast-receiver/utssdk/interface';

	kuxBroadcastReceiver.addAction({
	    actionName: 'android.intent.action.BATTERY_CHANGED' // 这里需要替换自己的广播动作名称
	    success: _ => {
	        console.log('添加成功');
	    },
	    fail: e => {
	        console.log('添加失败，错误码：' + e.errCode);
	    }
	} as AddActionOptions);
	```

+ **注册监听**

	```ts
	import { BroadcastNameItem, AddActionOptions } from '@/uni_modules/kux-broadcast-receiver/utssdk/interface';

	kuxBroadcastReceiver.registerReceiver({
	    broadcastNames: [
	        {
	            type: 'string',
	            name: 'example.com.test.XXX', // 这里需要替换为自己的监听名称
	        }
	    ] as BroadcastNameItem[],
	    success: _ => {
	        console.log('注册监听成功');
	    },
	    fail: e => {
	        console.log('添加失败，错误码：' + e.errCode);
	    }
	} as AddActionOptions);
	```

+ **监听广播**

	```ts
	kuxBroadcastReceiver.onReceiver((data) => {
	    console.log('监听广播触发新事件', data);
	});
	```

+ **取消监听**

	```ts
	import { UnregisterReceiverOptions } from '@/uni_modules/kux-broadcast-receiver/utssdk/interface';

	kuxBroadcastReceiver.unregisterReceiver({
	    success: _ => {
	        console.log('取消监听成功');
	    },
	    fail: e => {
	        console.log('取消监听失败，错误码：' + e.errCode);
	    }
	} as UnregisterReceiverOptions);
	```
> **提示**
> 
> 上面示例代码是为了演示每个方法所以做了拆分，实际使用中请根据具体情况自行引入插件库API和类型，完整使用请参考下面的示例。

<a id="advanced"></a>
## 进阶
因为注册监听时，`broadcastNames` 是数组形式的，所以一次可以接收多个广播类型的内容，这就给插件带来了巨大的灵活便利性了。下面监听手机电量变化为例进行演示。

<a id="advanced_BroadcastNameItem"></a>
### 获取电量变化

```html
<template>
	<view class="px-3 py-3">
		<view class="mb-3 flex-row justify-center">
			<text class="title" v-if="listening">当前电量监听：{{ batteryCount }}%</text>
		</view>
		<button type="primary" class="mb-3" :disabled="listening" @tap="addAction">开始监听电池电量变化</button>
		<button type="primary" class="mb-3" :disabled="!listening && !init" @tap="unregisterReceiver">取消监听</button>
	</view>
</template>

<script setup>
	import { useBroadcastReceiver } from '@/uni_modules/kux-broadcast-receiver';
	import { KuxBroadcastReceiver, AddActionOptions, RegisterReceiverOptions, BroadcastNameItem, UnregisterReceiverOptions } from '@/uni_modules/kux-broadcast-receiver/utssdk/interface';
	import BatteryManager from 'android.os.BatteryManager';
	
	const batteryCount = ref(0);
	
	const init = ref(false);
	const listening = ref(false);
	
	const kuxBroadcastReceiver: KuxBroadcastReceiver = useBroadcastReceiver();
	
	const toast = (msg: string) => {
		uni.showModal({
			title: '提示',
			content: msg,
			showCancel: false
		});
	}
	
	/**
	 * 注册监听
	 */
	const registerReceiver = () => {
		kuxBroadcastReceiver.registerReceiver({
			broadcastNames: [
				{
					type: 'int',
					name: BatteryManager.EXTRA_LEVEL,
					defaultValue: -1
				},
				{
					type: 'int',
					name: BatteryManager.EXTRA_SCALE,
					defaultValue: -1
				}
			] as BroadcastNameItem[],
			success: _ => {
				console.log('注册监听成功');
				listening.value = true;
				toast('注册监听成功');
			},
			fail: e => {
				console.log('注册监听失败，错误码：' + e.errCode);
				toast('注册监听失败，错误码：' + e.errCode);
			}
		} as RegisterReceiverOptions);
	}
	
	/**
	 * 添加广播动作
	 */
	const addAction = () => {
		if (listening.value) return;
		
		kuxBroadcastReceiver.addAction({
			actionName: 'android.intent.action.BATTERY_CHANGED',
			success: _ => {
				console.log('添加成功');
				// toast('添加')
				// 注册监听
				registerReceiver();
			},
			fail: e => {
				console.log('添加失败，错误码：' + e.errCode);
			}
		} as AddActionOptions);
	}
	
	/**
	 * 监听广播
	 */
	const onReceiver = () => {
		kuxBroadcastReceiver.onReceiver((data) => {
			console.log('监听广播触发新事件');
			batteryCount.value = data[0] as number;
			toast('监听到电量变化，新电量返回：' + data[0]);
		});
	}
	
	/**
	 * 取消监听
	 */
	const unregisterReceiver = () => {
		kuxBroadcastReceiver.unregisterReceiver({
			success: _ => {
				console.log('取消监听成功');
				listening.value = false;
				toast('取消监听成功');
			},
			fail: e => {
				console.log('取消监听失败，错误码：' + e.errCode);
				toast('取消监听失败，错误码：' + e.errCode);
			}
		} as UnregisterReceiverOptions);
	}
	
	onLoad((_) => {
		onReceiver();
	})
	
	onUnload(() => {
		unregisterReceiver();
	})
</script>

<style>
	.flex-row {
		flex-direction: row;
	}
	.justify-center {
		justify-content: center;
	}
	.items-center {
		align-items: center;
	}
	.mb-3 {
		margin-bottom: 30px;
	}
	.px-3 {
		padding-left: 30px;
		padding-right: 30px;
	}
	.py-3 {
		padding-top: 30px;
		padding-bottom: 30px;
	}
	.text-center {
		text-align: center;
	}
	.title {
		font-size: 18px;
		font-weight: bold;
	}
</style>
```

<a id="api"></a>
## API

<a id="api_addAction"></a>
### addAction

#### 功能描述
添加广播动作。

#### 参数

| 属性 | 类型 | 默认值 | 必填 | 说明 | 最低版本
| --- | --- | --- | --- | --- | ---
| actionName | string |  | 是 | 动作名称 |
| success | function |  | 否 | 接口调用成功的回调函数 |
| fail | function |  | 否 | 接口调用失败的回调函数 |
| complete | function |  | 否 | 接口调用结束的回调函数（调用成功、失败都会执行）|

#### 错误

| 错误码 | 错误信息 | 说明
| --- | --- | ---
| 9010001 | 指定广播动作不存在 | 指定广播动作不存在
| 9010002 | 指定监听不存在 | 指定监听不存在
| 9010003 | 其他系统异常 | 其余所有系统上报的异常

<a id="api_registerReceiver"></a>
### registerReceiver

#### 功能描述
注册监听。需要 `addAction` 调用成功后调用。

#### 参数

| 属性 | 类型 | 默认值 | 必填 | 说明 | 最低版本
| --- | --- | --- | --- | --- | ---
| broadcastNames | `BroadcastNameItem[]`  |  | 是 | 接收的广播内容 |
| success | function |  | 否 | 接口调用成功的回调函数 |
| fail | function |  | 否 | 接口调用失败的回调函数 |
| complete | function |  | 否 | 接口调用结束的回调函数（调用成功、失败都会执行）|

#### 错误

| 错误码 | 错误信息 | 说明
| --- | --- | ---
| 9010001 | 指定广播动作不存在 | 指定广播动作不存在
| 9010002 | 指定监听不存在 | 指定监听不存在
| 9010003 | 其他系统异常 | 其余所有系统上报的异常

<a id="api_onReceiver"></a>
### onReceiver

#### 功能描述
监听广播。

#### 参数

| 属性 | 类型 | 说明
| --- | --- | ---
| data | `any[]`  | 广播回调的数据数组，和 `broadcastNames` 长度一致   | 

#### 错误

| 错误码 | 错误信息 | 说明
| --- | --- | ---
| 9010001 | 指定广播动作不存在 | 指定广播动作不存在
| 9010002 | 指定监听不存在 | 指定监听不存在
| 9010003 | 其他系统异常 | 其余所有系统上报的异常

<a id="api_unregisterReceiver"></a>
### unregisterReceiver

#### 功能描述
取消监听。
> **提示**
> 
> 插件实现的是动态注册，一般会随着页面销毁自动销毁的，这个是为了手动取消监听提供的方法。

#### 参数

| 属性 | 类型 | 默认值 | 必填 | 说明 | 最低版本
| --- | --- | --- | --- | --- | ---
| success | function |  | 否 | 接口调用成功的回调函数 |
| fail | function |  | 否 | 接口调用失败的回调函数 |
| complete | function |  | 否 | 接口调用结束的回调函数（调用成功、失败都会执行）|

#### 错误

| 错误码 | 错误信息 | 说明
| --- | --- | ---
| 9010001 | 指定广播动作不存在 | 指定广播动作不存在
| 9010002 | 指定监听不存在 | 指定监听不存在
| 9010003 | 其他系统异常 | 其余所有系统上报的异常

<a id="interface"></a>
## 自定义类型
### ApiCommonSuccessCallback
```ts
export type ApiCommonSuccessCallback = {
	errCode: number,
	errMsg: string
}
```

### ApiFail
```ts
export type ApiFail = (err: UniError) => void;
```

### ApiComplete
```ts
export type ApiComplete = (res: any) => void;
```

### AddActionOptions
```ts
export type AddActionOptions = {
	actionName: string,
	success?: (res: ApiCommonSuccessCallback) => void,
	fail?: ApiFail,
	complete?: ApiComplete
}
```

### BroadcastNameType
```ts
export type BroadcastNameType = 'string' | 'int' | 'bool' | 'float' | 'double' | 'byte' | 'char' | 'long'
```

### BroadcastNameItem
```ts
export type BroadcastNameItem = {
	type: BroadcastNameType,
	name: any,
	defaultValue: any
}
```

### RegisterReceiverOptions
```ts
export type RegisterReceiverOptions = {
	broadcastNames: BroadcastNameItem[],
	success?: (res: ApiCommonSuccessCallback) => void,
	fail?: ApiFail,
	complete?: ApiComplete
}
```

### OnReceiverCallback
```ts
export type OnReceiverCallback = (data: any[]) => void;
```

### UnregisterReceiverOptions
```ts
export type UnregisterReceiverOptions = {
	success?: (res: ApiCommonSuccessCallback) => void,
	fail?: ApiFail,
	complete?: ApiComplete
}
```

<a id="unierrors"></a>
## 错误码规范
插件完全使用的通用错误码规范，具体 [点击查看](https://uniapp.dcloud.net.cn/tutorial/err-spec.html)

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

___
### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手
