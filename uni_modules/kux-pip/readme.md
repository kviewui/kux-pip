# kux-pip
`kux-pip` 是一个原生画中画的简单封装实现，可以实现简单类似视频软件的小窗播放效果。支持画中画窗口变化监听、自定义宽高比等。

## 开源地址：[https://gitcode.com/kviewui/kux-pip](https://gitcode.com/kviewui/kux-pip)


## 插件特色
+ 原生画中画实现
+ 窗口变化监听
+ 自定义宽高比
+  同时支持 `uniapp` 项目和 `uniapp x` 项目

## 目录结构
<ol>
	<li><a href="#guide">基础</a>
		<ol>
			<li><a href="#guide_installation">安装配置</a></li>
			<li><a href="#guide_guide">入门使用</a></li>
		</ol>
	</li>
	<li><a href="#api">API</a>
		<ol>
			<li><a href="#api_start">start</a></li>
			<li><a href="#api_checkPermission">checkPermission</a></li>
			<li><a href="#api_checkSupportPIP">checkSupportPIP</a></li>
			<li><a href="#api_updatePipActions">updatePipActions</a></li>
			<li><a href="#api_onPipActions">onPipActions</a></li>
			<li><a href="#api_onUserLeaveHint">onUserLeaveHint</a></li>
		</ol>
	</li>
	<li><a href="#interface">自定义类型</a>
		<ol>
			<li><a href="#interface_StartOptions">StartOptions</a></li>
		</ol>
	</li>
	<li><a href="unierror">错误码</a>
	</li>
</ol>

<a id="guide"></a>
## 基础

<a id="guide_installation"></a>
### 安装配置
本插件为完全的 `uni_modules` 插件，所以直接在 [插件市场](https://ext.dcloud.net.cn) 搜索 `kux-pip` 安装即可。

<a id="guide_guide"></a>
### 入门使用
> **注意**
> 
> 需要打包自定义基座方可正常使用

#### uniapp x项目示例
```
<template>
	<!-- #ifdef APP -->
	<scroll-view style="flex:1">
	<!-- #endif -->
		<!-- 状态栏 -->
		<view v-if="height == initHeight" class="status_bar"></view>
		<view>
			<video ref="videoRef" style="width: 100%;" :style="{height: height + 'px'}" :controls="controls" src="http://www.runoob.com/try/demo_source/mov_bbb.mp4" @play="onPlay" @pause="onPause" @ended="onEnded" @timeupdate="onTimeUpdate"></video>
		</view>
		<button @tap="enterPictureInPictureMode">开启画中画模式</button>
		<button @tap="onCheckPermission">检查权限是否被授予</button>
		<button @tap="onCheckSupportPIP">检查是否支持画中画.</button>
	<!-- #ifdef APP -->
	</scroll-view>
	<!-- #endif -->
</template>

<script setup>
	import { start, StartOptions, StartSuccess, checkPermission, checkSupportPIP, onUserLeaveHint, updatePipActions, onPipActions  } from '@/uni_modules/kux-pip';
	
	const videoRef = ref<UniVideoElement | null>(null);
	
	const initHeight = ref(200);
	const controls = ref(true);
	const height = ref(initHeight.value);
	const isPlaying = ref(false);
	const time = ref(0);
	
	/**
	 * 更新播放状态以及刷新画中画按钮状态
	 */
	const updatePlayState = (state: boolean) => {
		updatePipActions({
			isPlaying: state,
			success(res: StartSuccess) {
				console.log('修改成功');
			},
			fail(err: UniError) {
				console.log(err);
			}
		})
	}
	
	const onTimeUpdate = (event: UniVideoTimeUpdateEvent) => {
		time.value = event.detail.currentTime;
	}
	
	const onPlay = (event: UniEvent) => {
		isPlaying.value = true;
		updatePlayState(isPlaying.value);
	}
	
	const onPause = (event: UniEvent) => {
		isPlaying.value = false;
		updatePlayState(isPlaying.value);
	}
	
	const onEnded = (event: UniEvent) => {
		isPlaying.value = false;
		updatePlayState(isPlaying.value);
	}
	
	const msg = (content: string) => {
		uni.showToast({
			icon: 'none',
			title: content,
		});
	}
	
	/**
	 * 监听画中画按钮点击事件
	 */
	onPipActions((state: number) => {
		if (state == 1) {
			msg('点击了播放');
			videoRef.value?.play();
		} else if (state == 2) {
			msg('点击了暂停');
			videoRef.value?.pause();
		} else if (state == 3) {
			msg('快进1秒');
			videoRef.value?.seek(time.value + 1);
		} else if (state == 4) {
			msg('快退1秒');
			videoRef.value?.seek(time.value - 1);
		}
	});
	
	function enterPictureInPictureMode() {
		start({
			isPlaying: isPlaying.value,
			success(res: StartSuccess) {
				console.log(res, '成功回调');
			},
			fail(res: UniError) {
				console.log(res, '失败回调');
			},
			complete(res: any) {
				console.log(res, '完成回调');
			},
			stateChange(res) {
				if (res.height < initHeight.value) {
					height.value = res.height;
					controls.value = false;
				} else {
					height.value = initHeight.value;
					controls.value = true;
				}
			}
		} as StartOptions);
	}
	
	/**
	 * 监听用户离开当前页面事件
	 */
	onUserLeaveHint((params: UniActivityParams) => {
		// 如果返回页面结果是index2页面，则进入画中画模式
		if (params.pageRoute == 'pages/index2/index2') {
			enterPictureInPictureMode();
		}
	})
	
	const modal = (message: string) => {
		uni.showModal({
			title: '提示',
			content: message,
			showCancel: false
		})
	}
	
	const onCheckPermission = () => {
		try {
			if (checkPermission()) {
				modal('权限已获取');
			} else {
				modal('权限未获取');
			}
		} catch (err: UniError) {
			modal(`检查权限失败：${err.errMsg}`);
		}
	}
	
	const onCheckSupportPIP = () => {
		if (checkSupportPIP()) {
			modal('支持画中画');
		} else {
			modal('不支持画中画');
		}
	}
</script>

<style>
	.status_bar {
		height: var(--status-bar-height);
		width: 100%;
		background-color: black;
	}
</style>
```

#### uniapp 项目示例
```
<template>
	<!-- #ifdef APP -->
	<scroll-view style="flex:1">
	<!-- #endif -->
		<!-- 状态栏 -->
		<view v-if="height == initHeight" class="status_bar"></view>
		<view>
			<video id="video" ref="videoRef" :style="{height: height + 'px', width: width}" :controls="true" src="http://www.runoob.com/try/demo_source/mov_bbb.mp4" @play="onPlay" @pause="onPause" @ended="onEnded" @timeupdate="onTimeUpdate"></video>
		</view>
		<button @click="enterPictureInPictureMode">开启画中画模式</button>
		<button @tap="onCheckPermission">检查权限是否被授予</button>
		<button @tap="onCheckSupportPIP">检查是否支持画中画</button>
	<!-- #ifdef APP -->
	</scroll-view>
	<!-- #endif -->
</template>

<script lang="ts" setup>
	import { ref } from 'vue';
	import { start, StartOptions, StartSuccess, checkPermission, checkSupportPIP, onUserLeaveHint, updatePipActions, onPipActions } from '@/uni_modules/kux-pip';
	import { onReady } from '@dcloudio/uni-app';
	
	// const videoRef = ref<UniVideoElement | null>(null);
	
	const initHeight = ref(200);
	const height = ref(initHeight.value);
	const width = ref('100%');
	const controls = ref(true);
	const isPlaying = ref(false);
	const time = ref(0);
	
	const videoContext = ref(null);
	
	onReady(() => {
		videoContext.value = uni.createVideoContext('video');
	})
	
	/**
	 * 更新播放状态以及刷新画中画按钮状态
	 */
	const updatePlayState = (state: boolean) => {
		updatePipActions({
			isPlaying: state,
			success(res: StartSuccess) {
				console.log('修改成功');
			},
			fail(err: UniError) {
				console.log(err);
			}
		})
	}
	
	const onTimeUpdate = (event: UniVideoTimeUpdateEvent) => {
		time.value = event.detail.currentTime;
	}
	
	const onPlay = (event: UniEvent) => {
		isPlaying.value = true;
		updatePlayState(isPlaying.value);
	}
	
	const onPause = (event: UniEvent) => {
		isPlaying.value = false;
		updatePlayState(isPlaying.value);
	}
	
	const onEnded = (event: UniEvent) => {
		isPlaying.value = false;
		updatePlayState(isPlaying.value);
	}
	
	const msg = (content: string) => {
		uni.showToast({
			icon: 'none',
			title: content,
		});
	}
	
	/**
	 * 监听画中画按钮点击事件
	 */
	onPipActions((state: number) => {
		if (state == 1) {
			msg('点击了播放');
			videoRef.value?.play();
		} else if (state == 2) {
			msg('点击了暂停');
			videoRef.value?.pause();
		} else if (state == 3) {
			msg('快进1秒');
			videoRef.value?.seek(time.value + 1);
		} else if (state == 4) {
			msg('快退1秒');
			videoRef.value?.seek(time.value - 1);
		}
	});
	
	function enterPictureInPictureMode() {
		start({
			success(res: StartSuccess) {
				console.log(res, '成功回调');
			},
			fail(res) {
				console.log(res, '失败回调');
			},
			complete(res: any) {
				console.log(res, '完成回调');
			},
			stateChange(res) {
				if (res.height < initHeight.value) {
					height.value = res.height;
					width.value = `${res.width}px`;
					controls.value = false;
				} else {
					height.value = initHeight.value;
					width.value = '100%';
					controls.value = true;
				}
			}
		} as StartOptions);
	}
	
	/**
	 * 监听用户离开当前页面事件
	 */
	onUserLeaveHint((params: UniActivityParams) => {
		// 如果返回页面结果是index2页面，则进入画中画模式
		if (params.pageRoute == 'pages/index2/index2') {
			enterPictureInPictureMode();
		}
	})
	
	const modal = (message: string) => {
		uni.showModal({
			title: '提示',
			content: message,
			showCancel: false
		})
	}
	
	const onCheckPermission = () => {
		try {
			if (checkPermission()) {
				modal('权限已获取');
			} else {
				modal('权限未获取');
			}
		} catch (err: UniError) {
			modal(`检查权限失败：${err.errMsg}`);
		}
	}
	
	const onCheckSupportPIP = () => {
		if (checkSupportPIP()) {
			modal('支持画中画');
		} else {
			modal('不支持画中画');
		}
	}
</script>

<style>
	.status_bar {
		height: var(--status-bar-height);
		width: 100%;
		background-color: black;
	}
</style>
```
> **注意**
> 
> + 因为uniapp项目的activity管理和uniapp x的有差异，所以uniapp项目小窗是把整个应用小窗了，这点要特别注意下
> 
> + 需要小窗的页面建议禁用原生导航栏
> 
> + uniapp项目下需要自己调整进入小窗和退出小窗时页面内容的样式布局，可以参考上面示例代码和uniapp x的是有少许的差异
>
> + 编译器 `4.25` 及以上版本测试有些问题，报错：`uts.sdk.modules.kuxPipIndexKt.startByJs stateChange is not found`，该问题后续找官方协助解决

<a id="api"></a>
## API

<a id="api_start"></a>
### start
+ 描述：开启画中画
+ 参数：[StartOptions](#startoptions_values)
+ 返回值：`void`

<a id="api_checkpermission"></a>
### checkPermission
+ 描述：检查权限是否被授予
+ 参数：`void`
+ 返回值：`boolean`
+ 注意事项：
	+ 该方法如果权限检查失败会抛出 `UniError` 异常，所以需要用 `try...catch` 捕获异常
	+ `v1.0.2` 及以上版本支持

+ 示例：

	```
	try {
		const permission = checkPermission();
		if (permission) {
			console.log('权限已获取');
		} else {
			console.log('权限未获取');
		}
	} catch (err: UniError) {
		console.log(`检查权限失败：${err.errMsg}`);
	}
	```

<a id="api_checksupportpip"></a>
### checkSupportPIP
+ 描述：检查是否支持画中画
+ 参数：`void`
+ 返回值：`boolean`
+ 注意事项：
	+ `v1.0.2` 及以上版本支持
	
+ 示例：

	```
	const support = checkSupportPIP();
	if (support) {
		console.log('支持画中画');
	} else {
		console.log('不支持画中画');
	}
	```

<a id="api_updatepipactions"></a>
### updatePipActions
+ 描述：更新画中画操作按钮
+ 参数：[updatePipActions](#updatepipactions_values)
+ 返回值：`void`
+ 注意事项：
	+ `v1.0.3` 及以上版本支持

<a id="api_onpipactions"></a>
### onPipActions
+ 描述：监听画中画操作按钮点击事件
+ 参数：`(state: number) => void`
+ 返回值：`void`
+ state: 1 播放，2 暂停，3 快进，4 快退
+ 注意事项：
	+ `v1.0.3` 及以上版本支持


<a id="api_onuserleaveshint"></a>
### onUserLeaveHint
+ 描述：监听用户离开小窗事件
+ 参数：`(params: UniActivityParams) => void`
+ 返回值：`void`
+ 注意事项：
	+ `v1.0.3` 及以上版本支持

<a id="updatepipactions_values"></a>
#### updatePipActions参数
| 参数名 | 类型 | 是否必填 | 默认值 | 描述
| --- | --- | --- | --- | ---
| isPlaying | `boolean` | 否 | `false` | 是否正在播放视频
| success | (res: [StartSuccess](#startsuccess_values)) => void | 否 | - | 成功回调
| fail | (res: `UniError`) => void | 否 | - | 失败回调
| complete | (res: `any`) => void | 否 | - | 完成回调

<a id="startoptions_values"></a>
#### StartOptions
| 参数名 | 类型 | 是否必填 | 默认值 | 描述
| --- | --- | --- | --- | ---
| numerator | `number` | 否 | `16` | 自定义像素比的分子或者长度比例
| denominator | `number` | 否 | `9` | 自定义像素比的分母或者宽度比例
| success | (res: [StartSuccess](#startsuccess_values)) => void | 否 | - | 成功回调
| fail | (res: `UniError`) => void | 否 | - | 失败回调
| complete | (res: `any`) => void | 否 | - | 完成回调
| stateChange | (res: [StateChange](#statechange_values)) => void | 否 | - | 窗口变化监听回调

<a id="startsuccess_values"></a>
#### StartSuccess
| 参数名 | 类型 | 描述
| --- | --- | ---
| errCode | `number` | 成功状态码
| errMsg | `string` | 成功描述

<a id="statechange_values"></a>
#### StateChange
| 参数名 | 类型 | 描述
| --- | --- | ---
| width | `number` | 画中画窗口宽度
| height | `number` | 画中画窗口高度

<a id="interface"></a>
## 自定义类型

<a id="interface_StartSuccess"></a>
### StartSuccess
```
export type StartSuccess = {
	errCode: number;
	errMsg: string;
}
```

<a id="interface_StateChange"></a>
### StateChange
```
export type StateChange = {
	width: number;
	height: number;
}
```

<a id="interface_StartOptions"></a>
### StartOptions
```
export type StartOptions = {
	numerator?: number;
	denominator?: number;
	success?: (res: StartSuccess) => void;
	fail?: (res: UniError) => void;
	complete?: (res: any) => void;
	stateChange?: (res: StateChange) => void;
}
```

<a id="interface_Start"></a>
### Start
```
export type Start = (options: StartOptions) => void;
```

<a id="interface_StartErrorCode"></a>
### StartErrorCode
```
export type StartErrorCode = 1001 | 1002;
```

<a id="interface_StartFail"></a>
### StartFail
```
export interface StartFail extends IUniError {
	errCode: StartErrorCode
}
```

<a id="unierror"></a>
## 错误码
| 错误码 | 描述
| --- | ---
| 1001 | 开启画中画失败
| 1002 | 用户拒绝了画中画授权
| 1003 | 当前系统版本不支持画中画
| 1004 | 当前不是画中画状态，无法更新画中画按钮 (v1.0.3新增)

___
### 友情推荐
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手