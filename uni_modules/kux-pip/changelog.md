## 1.0.3（2024-12-06）
+ 新增视频控制条，支持播放、暂停、快进和快退。
+ 新增 `updatePipActions` API，用于更新画中画的播放/暂停操作按钮。
+ 新增 `onPipActions` API，用于监听画中画的播放/暂停/快进/快退操作按钮点击事件。
+ 新增 `onUserLeaveHint` API，用于监听用户离开当前页面时触发的回调。
+ `StartOptions` 新增 `isPlaying` 是否正在播放参数，用于切换画中画播放暂停按钮状态。
+ 新增 绑定插件 [kux-broadcast-receiver](https://ext.dcloud.net.cn/plugin?id=17040)，用于监听系统广播。
## 1.0.2（2024-08-28）
+ 修复新版编译器报错 `activity` 找不到的问题。
+ 新增 `checkPermission` API，用于检查是否获取了权限。
+ 新增 `checkSupportPIP` API，用于检查当前设备系统是否支持PIP画中画功能。
+ 错误码新增 `1003`，表示当前设备不支持PIP画中画功能。
+ 优化其他已知问题。

## 1.0.1（2024-03-08）
优化错误反馈，新增源错误返回

## 1.0.0（2024-03-06）
初始发布
