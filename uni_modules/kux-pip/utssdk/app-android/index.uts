import Build from 'android.os.Build';
import Rational from 'android.util.Rational';
import PictureInPictureParams from 'android.app.PictureInPictureParams';
import RemoteAction from 'android.app.RemoteAction';
import Icon from 'android.graphics.drawable.Icon';
import PendingIntent from 'android.app.PendingIntent';
import Intent from 'android.content.Intent';
import BroadcastReceiver from 'android.content.BroadcastReceiver';
import IntentFilter from 'android.content.IntentFilter';
import Context from 'android.content.Context';

import { StartOptions, StartSuccess, StateChange, OnUserLeaveHintCallback, UpdatePipActionsOptions, OnPipActionsCallback } from '../interface';
import { StartFailImpl } from '../unierror';
import Activity from 'android.app.Activity';
import Configuration from 'android.content.res.Configuration';
import R from 'android.R';
import { useBroadcastReceiver, KuxBroadcastReceiver, AddActionOptions, BroadcastNameItem, RegisterReceiverOptions } from '@/uni_modules/kux-broadcast-receiver';

const ACTION_PLAY = 'cn.uvuejs.kux.kuxPip.PLAY';
const ACTION_PAUSE = 'cn.uvuejs.kux.kuxPip.PAUSE';
const ACTION_FAST_FORWARD = 'cn.uvuejs.kux.kuxPip.FAST_FORWARD';
const ACTION_FAST_REWIND = 'cn.uvuejs.kux.kuxPip.REWIND';
const kuxBroadcastReceiver: KuxBroadcastReceiver = useBroadcastReceiver();
let _isPlaying = false;
let onUserLeaveHintCallback: OnUserLeaveHintCallback | null = null;
let onPipActionsCallback: OnPipActionsCallback | null = null;
let _selfActivity: Activity | null = null;
const pipParams: PictureInPictureParams.Builder = new PictureInPictureParams.Builder();

export class UTSActivityCallback extends UniActivityCallback {
	constructor() {
	    super();
	}
	override onUserLeaveHint(params: UniActivityParams) {
		if (onUserLeaveHintCallback != null) {
			onUserLeaveHintCallback!(params);
		}
	}
}

export function onUserLeaveHint(callback: OnUserLeaveHintCallback) {
	onUserLeaveHintCallback = callback;
	UTSAndroid.onActivityCallback(new UTSActivityCallback());
}

export function onPipActions(callback: OnPipActionsCallback) {
	onPipActionsCallback = callback;
}

function createPipActions() : List<RemoteAction> {
	const _this = UTSAndroid.getAppContext();
	const actions = mutableListOf(
		new RemoteAction(
			Icon.createWithResource(_this, R.drawable.ic_media_rew),
			"Rewind",
			"Rewind the video",
			PendingIntent.getBroadcast(_this, 0, new Intent(ACTION_FAST_REWIND), PendingIntent.FLAG_IMMUTABLE)
		),
		new RemoteAction(
			Icon.createWithResource(_this, _isPlaying ? R.drawable.ic_media_pause : R.drawable.ic_media_play),
			"Play Or Pause",
			"Play Or Pause the video",
			PendingIntent.getBroadcast(_this, 0, new Intent(_isPlaying ? ACTION_PAUSE : ACTION_PLAY), PendingIntent.FLAG_IMMUTABLE)
		),
		new RemoteAction(
			Icon.createWithResource(_this, R.drawable.ic_media_ff),
			"Fast Forward",
			"Fast Forward the video",
			PendingIntent.getBroadcast(_this, 0, new Intent(ACTION_FAST_FORWARD), PendingIntent.FLAG_IMMUTABLE)
		)
	);

	return actions;
}

export function updatePipActions(options: UpdatePipActionsOptions) {
	_isPlaying = options.isPlaying ?? false;
	if (!checkSupportPIP()) {
		let res = new StartFailImpl(1003);
		options.fail?.(res);
		options.complete?.(res);
	} else if (!_selfActivity!.isInPictureInPictureMode()) {
		let res = new StartFailImpl(1004);
		options.fail?.(res);
		options.complete?.(res);
	} else {
		console.log(_isPlaying);
		pipParams
			.setActions(createPipActions())
			.build();
		_selfActivity!.setPictureInPictureParams(pipParams.build());
		const res : StartSuccess = {
			errCode: 0,
			errMsg: 'updatePipActions:ok'
		};
		options.success?.(res);
		options.complete?.(res);
	}
}

function enterPictureInPictureMode(numerator : number, denominator : number, options : StartOptions) {
	const aspectRatio = new Rational(numerator.toInt(), denominator.toInt());
	pipParams
		.setActions(createPipActions())
		.setAspectRatio(aspectRatio)
		.build();
	_selfActivity = UTSAndroid.getUniActivity()!;
	_selfActivity!.enterPictureInPictureMode(pipParams.build());
	const res : StartSuccess = {
		errCode: 0,
		errMsg: 'start:ok'
	}
	
	kuxBroadcastReceiver.addAction({
		actionName: ACTION_PLAY
	} as AddActionOptions);
	kuxBroadcastReceiver.addAction({
		actionName: ACTION_PAUSE
	} as AddActionOptions);
	kuxBroadcastReceiver.addAction({
		actionName: ACTION_FAST_FORWARD
	} as AddActionOptions);
	kuxBroadcastReceiver.addAction({
		actionName: ACTION_FAST_REWIND
	} as AddActionOptions);
	kuxBroadcastReceiver.registerReceiver({
		broadcastNames: [
			{
				type: 'string',
				name: 'cn.uvuejs.kux.kuxPip',
				defaultValue: ''
			}
		] as BroadcastNameItem[]
	} as RegisterReceiverOptions)
	
	kuxBroadcastReceiver.onActionReceiver((data: string) => {
		if (data == ACTION_PLAY) {
			onPipActionsCallback?.(1);
		} else if (data == ACTION_PAUSE) {
			onPipActionsCallback?.(2);
		} else if (data == ACTION_FAST_FORWARD) {
			onPipActionsCallback?.(3);
		} else if (data == ACTION_FAST_REWIND) {
			onPipActionsCallback?.(4);
		}
	})
	
	options.success?.(res);
	options.complete?.(res);
}

const permissionCheck = ["android.permission.FOREGROUND_SERVICE", "android.permission.SYSTEM_ALERT_WINDOW"]

/**
 * 检查当前系统版本是否支持PIP
 */
export function checkSupportPIP() : boolean {
	return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
}

/**
 * 检查权限是否被授予
 */
export function checkPermission() : boolean {
	if (checkSupportPIP()) {
		return UTSAndroid.checkSystemPermissionGranted(UTSAndroid.getUniActivity()!, permissionCheck);
	} else {
		const res = new StartFailImpl(1003);
		throw res;
	}
}

export function start(options : StartOptions) {
	if (checkSupportPIP()) {
		// let permission = ["android.permission.FOREGROUND_SERVICE", "android.permission.SYSTEM_ALERT_WINDOW"];
		const numerator = options?.numerator ?? 16;
		const denominator = options?.denominator ?? 9;
		try {
			UTSAndroid.requestSystemPermission(UTSAndroid.getUniActivity()!, permissionCheck, (allRight : boolean, grantedList : string[]) => {
				if (allRight) {
					// console.log('用户同意了全部权限');
					enterPictureInPictureMode(numerator, denominator, options);
				} else {
					// console.log('用户仅同意了grantedList中的权限');
					enterPictureInPictureMode(numerator, denominator, options);
				}
			}, (doNotAskAgain : boolean, grantedList : string[]) => {
				if (doNotAskAgain) {
					// console.log('用户拒绝了权限，并且选择不再询问');
					let res = new StartFailImpl(1002);
					res.cause = new SourceError('用户拒绝了权限，并且选择不再询问');
					options.fail?.(res);
					options.complete?.(res);
				}
			})
		} catch (e) {
			let res = new StartFailImpl(1001);
			res.cause = new SourceError(e.message ?? '');
			options.fail?.(res);
			options.complete?.(res);
		}
	} else {
		let res = new StartFailImpl(1003);
		options.fail?.(res);
		options.complete?.(res);
	}

	// const screenWidth = 

	UTSAndroid.onAppConfigChange((res) => {
		const widthDp = res.get('screenWidthDp') as number;
		const heightDp = res.get('screenHeightDp') as number;
		const stateChange : StateChange = {
			width: widthDp,
			height: heightDp
		};
		options.stateChange?.(stateChange);
	})
}

// 导出类型
export * from '../interface';

// class MyActivity extends Activity {
// 	constructor() {
// 		super();
// 	}
// 	override onPictureInPictureModeChanged (isInPictureInPictureMode: boolean, newConfig: Configuration): void {
// 		super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig);
// 		console.log(isInPictureInPictureMode);
// 	}
// }